package viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.phareal.lambda.R;

/**
 * Created by Phareal on 01/12/2016.
 */
public class ProfilViewHolder extends RecyclerView.ViewHolder {
    private RelativeLayout profil_item_rl;
    private TextView profil_item_name;
    private TextView profil_item_competence;
    private ImageView profil_item_photo;

    public ProfilViewHolder(View itemView) {
        super(itemView);
        this.profil_item_rl = (RelativeLayout) itemView.findViewById(R.id.profil_item_rl);
        this.profil_item_name = (TextView) itemView.findViewById(R.id.name);
        this.profil_item_competence = (TextView) itemView.findViewById(R.id.competence);
        this.profil_item_photo = (ImageView) itemView.findViewById(R.id.photo);
    }

    public TextView getProfil_item_name() {
        return profil_item_name;
    }

    public TextView getProfil_item_competence() {
        return profil_item_competence;
    }

    public ImageView getProfil_item_photo() {
        return profil_item_photo;
    }
}
