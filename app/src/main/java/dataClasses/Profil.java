package dataClasses;

import java.io.Serializable;

/**
 * Created by Phareal on 01/12/2016.
 */
public class Profil implements Serializable {
    private String name, competence;

    public Profil(String name, String competence) {
        this.setName(name);
        this.setCompetence(competence);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompetence() {
        return competence;
    }

    public void setCompetence(String competence) {
        this.competence = competence;
    }
}
