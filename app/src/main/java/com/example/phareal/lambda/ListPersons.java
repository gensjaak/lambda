package com.example.phareal.lambda;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.phareal.lambda.db.DaoMaster;
import com.example.phareal.lambda.db.DaoSession;
import com.example.phareal.lambda.db.PROFIL;
import com.example.phareal.lambda.db.PROFILDao;

import java.util.ArrayList;
import java.util.List;

import dataClasses.Profil;
import viewadapter.ProfilViewAdapter;

public class ListPersons extends AppCompatActivity {
    private static final int SHOULDRETURNPROFILOBJECT = 1;
    public static ArrayList<Profil> profils = new ArrayList<Profil>() {
    };
    public static ProfilViewAdapter adapter = null;

    private static PROFILDao profil_dao;
    private static PROFIL temp_profil_object;
    private final String DB_NAME = "profils_db";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_persons);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), NewPerson.class);
                startActivityForResult(intent, SHOULDRETURNPROFILOBJECT);
            }
        });

        profil_dao = setupDB();
    }

    private PROFILDao setupDB() {
        DaoMaster.DevOpenHelper masterHelper = new DaoMaster.DevOpenHelper(this, DB_NAME, null);
        SQLiteDatabase db = masterHelper.getWritableDatabase();
        DaoMaster master = new DaoMaster(db);
        DaoSession masterSession = master.newSession();
        return masterSession.getPROFILDao();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SHOULDRETURNPROFILOBJECT) {
            Profil profilToAdd = (Profil) data.getSerializableExtra("PROFILTOADD");
            profils.add(profilToAdd);
            adapter.notifyDataSetChanged();

            // Save to DB
            temp_profil_object = new PROFIL(null, profilToAdd.getName(), profilToAdd.getCompetence());
            saveToDB(temp_profil_object);
        }
    }

    private void saveToDB(PROFIL temp_profil_object) {
        profil_dao.insert(temp_profil_object);
    }

    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        View rootView;

        private ArrayList<Integer> photos = new ArrayList<Integer>() {
        };
        private RecyclerView rv_profils = null;
        private LinearLayoutManager llm_profils;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_profils, container, false);

            if (getArguments().getInt(ARG_SECTION_NUMBER) == 1) {
                rootView = inflater.inflate(R.layout.fragment_profils, container, false);

                // Lister les profils
                profilsListing();
            } else if (getArguments().getInt(ARG_SECTION_NUMBER) == 2) {
                rootView = inflater.inflate(R.layout.fragment_offres, container, false);
            }

            return rootView;
        }

        private void profilsListing() {
            rv_profils = (RecyclerView) rootView.findViewById(R.id.rv_profils);
            rv_profils.setHasFixedSize(true);
            llm_profils = new LinearLayoutManager(rootView.getContext());
            rv_profils.setLayoutManager(llm_profils);

            // Bind data to view before show it
            bindData();

            // Show all to user
            adapter = new ProfilViewAdapter(profils, photos);
            rv_profils.setAdapter(adapter);
        }

        private void bindData() {
            photos.add(R.mipmap.people1);
            photos.add(R.mipmap.people2);
            photos.add(R.mipmap.people3);
            photos.add(R.mipmap.people4);
            photos.add(R.mipmap.people5);
            photos.add(R.mipmap.people6);
            photos.add(R.mipmap.people7);
            photos.add(R.mipmap.people8);
            photos.add(R.mipmap.people9);
            photos.add(R.mipmap.people10);

            getFromDB();
        }

        private void getFromDB() {
            List<PROFIL> profilsFromDB = profil_dao.loadAll();
            profils = new ArrayList<Profil>() {
            };
            if (profilsFromDB.size() > 0) {
                for (int i = 0; i < profilsFromDB.size(); i++) {
                    profils.add(new Profil(profilsFromDB.get(i).getName(), profilsFromDB.get(i).getCompetence()));
                }
            }
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "PROFILS";
                case 1:
                    return "OFFRES";
            }
            return null;
        }
    }
}
