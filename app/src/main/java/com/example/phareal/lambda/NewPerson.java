package com.example.phareal.lambda;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import dataClasses.Profil;

public class NewPerson extends AppCompatActivity {
    private EditText new_profil_competence, new_profil_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_person);

        new_profil_competence = (EditText) findViewById(R.id.new_profil_competence);
        new_profil_name = (EditText) findViewById(R.id.new_profil_name);
    }

    public void addPersonFunction(View v) {
        if (!new_profil_name.getText().toString().equals("") && !new_profil_competence.getText().toString().equals("")) {
            Intent resultIntent = new Intent();
            resultIntent.putExtra("PROFILTOADD", new Profil(new_profil_name.getText().toString(), new_profil_competence.getText().toString()));
            setResult(ListPersons.RESULT_OK, resultIntent);
            finish();
        }
    }
}
