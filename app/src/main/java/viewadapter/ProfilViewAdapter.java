package viewadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.phareal.lambda.R;

import java.util.ArrayList;

import dataClasses.Profil;
import viewholder.ProfilViewHolder;

/**
 * Created by Phareal on 01/12/2016.
 */
public class ProfilViewAdapter extends RecyclerView.Adapter<ProfilViewHolder> {
    ArrayList<Profil> profils = new ArrayList<Profil>();
    ArrayList<Integer> photos = new ArrayList<Integer>();
    private Integer strictPosition = 0;

    public ProfilViewAdapter(ArrayList<Profil> profils, ArrayList<Integer> photos) {
        this.profils = profils;
        this.photos = photos;
    }

    @Override
    public ProfilViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_profil, viewGroup, false);
        ProfilViewHolder pvh = new ProfilViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(ProfilViewHolder holder, int position) {
        int descOrder = (this.getItemCount() - position) - 1;
        holder.getProfil_item_competence().setText(this.profils.get(descOrder).getCompetence());
        holder.getProfil_item_name().setText(this.profils.get(descOrder).getName());
        if (strictPosition >= this.photos.size()) strictPosition = 0;
        holder.getProfil_item_photo().setImageResource(photos.get(strictPosition));
        strictPosition++;
    }

    @Override
    public int getItemCount() {
        return this.profils.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
