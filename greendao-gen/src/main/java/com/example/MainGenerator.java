package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MainGenerator {
    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "com.example.phareal.lambda.db");

        Entity profile_entity = schema.addEntity("PROFIL");

        profile_entity.addIdProperty();
        profile_entity.addStringProperty("name").notNull();
        profile_entity.addStringProperty("competence").notNull();

        new DaoGenerator().generateAll(schema, "./app/src/main/java");
    }
}